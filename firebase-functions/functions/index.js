const functions = require("firebase-functions");
const app = require("express")();

const admin = require("firebase-admin");
admin.initializeApp();

const firebase = require("firebase");
const firebaseConfig = {
    apiKey: "AIzaSyBcsnP-YT9jLz_IJbcUwerawKP8ZUzTRcQ",
    authDomain: "jodel-web-concept.firebaseapp.com",
    databaseURL: "https://jodel-web-concept.firebaseio.com",
    projectId: "jodel-web-concept",
    storageBucket: "jodel-web-concept.appspot.com",
    messagingSenderId: "790979767877",
    appId: "1:790979767877:web:c2bc0dd9e22a8a59"
};
firebase.initializeApp(firebaseConfig);

const db = admin.firestore();

app.get("/getJodel", (request, response) => {
    db.collection("jodel")
        .orderBy("createdAt", "desc")
        .get()
        .then(data => {
            let jodel = [];
            data.forEach(doc => {
                jodel.push({
                    jodelId: doc.id,
                    ...doc.data()
                });
            });
            return response.json(jodel);
        })
        .catch(err => console.error(err));
}); // gets all jodels in the database

app.post("/createJodel", (request, response) => {
    const newJodel = {
        body: request.body.body,
        userHandle: request.body.userHandle,
        createdAt: new Date().toISOString()
    };

    db.collection("jodel")
        .add(newJodel)
        .then(doc => {
            response.json({
                message: `document ${doc.id} created successfully`
            });
        })
        .catch(err => {
            response.status(500).json({ error: "something went wrong" });
            console.error(err);
        });
}); //creates new jodel

//sign-up route
app.post("/signup", (request, response) => {
    firebase
        .auth()
        .signInAnonymously()
        .onAuthStateChanged(function(user) {
            if (user) {
                const newUser = {
                    userId: user.uid,
                    random: "random",
                    test: "test123"
                };

                db.collection("users")
                    .add(newUser)
                    .then(doc => {
                        response.json({
                            message: `document ${doc.id} created successfully`
                        });
                    })
                    .catch(err => {
                        response
                            .status(500)
                            .json({ error: "something went wrong" });
                        console.error(err);
                    });
                return response.status(201).json({
                    message: `user ${data.user.uid} signed up successfully`
                });
            }
        })
        .catch(err => {
            console.error(err);
            return response.status(500).json({ error: err.code });
        });
});

exports.api = functions.region("europe-west1").https.onRequest(app);
